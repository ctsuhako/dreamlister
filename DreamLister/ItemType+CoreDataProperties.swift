//
//  ItemType+CoreDataProperties.swift
//  DreamLister
//
//  Created by Clay Tsuhako on 9/15/16.
//  Copyright © 2016 Clay Tsuhako. All rights reserved.
//

import Foundation
import CoreData

extension ItemType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemType> {
        return NSFetchRequest<ItemType>(entityName: "ItemType");
    }

    @NSManaged public var type: String?
    @NSManaged public var toItem: Item?

}
