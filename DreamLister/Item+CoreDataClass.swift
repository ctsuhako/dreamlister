//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Clay Tsuhako on 9/15/16.
//  Copyright © 2016 Clay Tsuhako. All rights reserved.
//

import Foundation
import CoreData


public class Item: NSManagedObject {
    
    public override func awakeFromInsert() {
        
        super.awakeFromInsert()
        
        self.created = NSDate()
        
    }

}
